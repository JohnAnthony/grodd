package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTokeniser(t *testing.T) {
	testCases := []struct {
		description string
		fileRef     string
		input       string
		expect      []Token
	}{
		{
			description: "empty",
			input:       "",
			expect: []Token{
				{TokTypEOF, "", 1},
			},
		},
		{
			description: "individual tokens",
			input:       "'(())",
			expect: []Token{
				{TokTypQuote, "'", 1},
				{TokTypParenOpen, "(", 2},
				{TokTypParenOpen, "(", 3},
				{TokTypParenClose, ")", 4},
				{TokTypParenClose, ")", 5},
				{TokTypEOF, "", 6},
			},
		},
		{
			description: "string",
			input:       `"hello world"`,
			expect: []Token{
				{TokTypString, `"hello world"`, 1},
				{TokTypEOF, "", 14},
			},
		},
		{
			description: "wind through whitespace",
			input: "  	 	 '(  	 	 ( )  	 )   	",
			expect: []Token{
				{TokTypQuote, "'", 7},
				{TokTypParenOpen, "(", 8},
				{TokTypParenOpen, "(", 15},
				{TokTypParenClose, ")", 17},
				{TokTypParenClose, ")", 22},
				{TokTypEOF, "", 27},
			},
		},
		{
			description: "strings",
			input:       `'("one" "two" "three" ("four" ("five")))`,
			expect: []Token{
				{TokTypQuote, "'", 1},
				{TokTypParenOpen, "(", 2},
				{TokTypString, `"one"`, 3},
				{TokTypString, `"two"`, 9},
				{TokTypString, `"three"`, 15},
				{TokTypParenOpen, "(", 23},
				{TokTypString, `"four"`, 24},
				{TokTypParenOpen, "(", 31},
				{TokTypString, `"five"`, 32},
				{TokTypParenClose, ")", 38},
				{TokTypParenClose, ")", 39},
				{TokTypParenClose, ")", 40},
				{TokTypEOF, "", 41},
			},
		},
		{
			description: "symbol",
			input:       "foo",
			expect: []Token{
				{TokTypSymbol, "foo", 1},
				{TokTypEOF, "", 4},
			},
		},
		{
			description: "with symbols",
			input:       "(define square (lambda (x) (* x x)))",
			expect: []Token{
				{TokTypParenOpen, "(", 1},
				{TokTypSymbol, "define", 2},
				{TokTypSymbol, "square", 9},
				{TokTypParenOpen, "(", 16},
				{TokTypSymbol, "lambda", 17},
				{TokTypParenOpen, "(", 24},
				{TokTypSymbol, "x", 25},
				{TokTypParenClose, ")", 26},
				{TokTypParenOpen, "(", 28},
				{TokTypSymbol, "*", 29},
				{TokTypSymbol, "x", 31},
				{TokTypSymbol, "x", 33},
				{TokTypParenClose, ")", 34},
				{TokTypParenClose, ")", 35},
				{TokTypParenClose, ")", 36},
				{TokTypEOF, "", 37},
			},
		},
		{
			description: "with file reference",
			input:       "(* foo bar)",
			fileRef:     "foo.scm",
			expect: []Token{
				{TokTypParenOpen, "(", 1},
				{TokTypSymbol, "*", 2},
				{TokTypSymbol, "foo", 4},
				{TokTypSymbol, "bar", 8},
				{TokTypParenClose, ")", 11},
				{TokTypEOF, "", 12},
			},
		},
		{
			description: "integers",
			input:       "(* (+ 2 4) 6)",
			expect: []Token{
				{TokTypParenOpen, "(", 1},
				{TokTypSymbol, "*", 2},
				{TokTypParenOpen, "(", 4},
				{TokTypSymbol, "+", 5},
				{TokTypInt, "2", 7},
				{TokTypInt, "4", 9},
				{TokTypParenClose, ")", 10},
				{TokTypInt, "6", 12},
				{TokTypParenClose, ")", 13},
				{TokTypEOF, "", 14},
			},
		},
		{
			description: "float",
			input:       "(* (+ 2.51 4.663) 6.919182)",
			expect: []Token{
				{TokTypParenOpen, "(", 1},
				{TokTypSymbol, "*", 2},
				{TokTypParenOpen, "(", 4},
				{TokTypSymbol, "+", 5},
				{TokTypFloat, "2.51", 7},
				{TokTypFloat, "4.663", 12},
				{TokTypParenClose, ")", 17},
				{TokTypFloat, "6.919182", 19},
				{TokTypParenClose, ")", 27},
				{TokTypEOF, "", 28},
			},
		},
		{
			description: "booleans",
			input:       "#t #f #true #false #T #F #TruE #FALSe",
			expect: []Token{
				{TokTypBool, "#t", 1},
				{TokTypBool, "#f", 4},
				{TokTypBool, "#true", 7},
				{TokTypBool, "#false", 13},
				{TokTypBool, "#T", 20},
				{TokTypBool, "#F", 23},
				{TokTypBool, "#TruE", 26},
				{TokTypBool, "#FALSe", 32},
				{TokTypEOF, "", 38},
			},
		},
		{
			description: "characters",
			input:       `#\a #\b #\A #\( #\  #\space #\newline #\NeWLinE`,
			expect: []Token{
				{TokTypChar, `#\a`, 1},
				{TokTypChar, `#\b`, 5},
				{TokTypChar, `#\A`, 9},
				{TokTypChar, `#\(`, 13},
				{TokTypChar, `#\ `, 17},
				{TokTypChar, `#\space`, 21},
				{TokTypChar, `#\newline`, 29},
				{TokTypChar, `#\NeWLinE`, 39},
				{TokTypEOF, "", 48},
			},
		},
		{
			description: "hex encoded characters",
			input:       `#\x49 #\x127 #\x3bb`,
			expect: []Token{
				{TokTypChar, `#\x49`, 1},
				{TokTypChar, `#\x127`, 7},
				{TokTypChar, `#\x3bb`, 14},
				{TokTypEOF, "", 20},
			},
		},
	}

	for _, tc := range testCases {
		tc := tc // Capture
		t.Run(tc.description, func(t *testing.T) {
			t.Parallel()

			tok := NewTokeniser(strings.NewReader(tc.input), tc.fileRef)
			results := make([]Token, 0)
			for {
				tok, err := tok.NextToken()
				require.Nil(t, err, tok)
				results = append(results, tok)
				if tok.Typ == TokTypEOF {
					break
				}
			}

			assert.Equal(t, tc.expect, results)
		})
	}
}
