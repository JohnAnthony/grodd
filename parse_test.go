package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParser(t *testing.T) {
	testCases := []struct {
		description string
		fileRef     string
		input       []Token
		expect      ParseTree
	}{
		{
			description: "empty",
			input:       []Token{{TokTypEOF, "", 1}},
			expect:      ParseTree{},
		},
	}

	for _, tc := range testCases {
		tc := tc // Capture
		t.Run(tc.description, func(t *testing.T) {
			t.Parallel()

			parser := NewParser()
			for _, tok := range tc.input {
				err := parser.Parse(tok)
				require.Nil(t, err, tok)
			}

			assert.Equal(t, tc.expect, parser.Root)
		})
	}
}
