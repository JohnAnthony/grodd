package main

var namedCharacters = map[string]rune{
	"alarm":     7,
	"backspace": 8,
	"delete":    127,
	"esc":       27,
	"linefeed":  10,
	"newline":   10,
	"nul":       0,
	"return":    13,
	"space":     32,
	"tab":       9,
	"vtab":      11,
}

type Parser struct {
	Root ParseTree
}

type ParseTree struct{}

func NewParser() *Parser {
	return &Parser{}
}

func (p *Parser) Parse(Token) error {
	return nil
}
