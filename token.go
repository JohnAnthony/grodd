package main

import (
	"bufio"
	"errors"
	"io"
	"strconv"
	"strings"
)

type TokenTyp int

const (
	TokTypBool TokenTyp = iota
	TokTypChar
	TokTypEOF
	TokTypFloat
	TokTypInt
	TokTypInvalid
	TokTypParen
	TokTypParenClose
	TokTypParenOpen
	TokTypQuote
	TokTypString
	TokTypSymbol
)

type Token struct {
	Typ TokenTyp
	Lit string
	Pos uint
}

var delim = map[rune]struct{}{
	rune(' '):  {},
	rune('"'):  {},
	rune('('):  {},
	rune(')'):  {},
	rune('\n'): {},
	rune('\t'): {},
}

type Tokeniser struct {
	Pos uint
	r   *bufio.Reader
}

func NewTokeniser(r io.Reader, ref string) *Tokeniser {
	return &Tokeniser{
		Pos: 0,
		r:   bufio.NewReader(r),
	}
}

func (t *Tokeniser) NextToken() (Token, error) {
	var ch rune
	var err error

	// Read character skipping over whitespace
	for {
		t.Pos++

		ch, _, err = t.r.ReadRune()
		if err == io.EOF {
			return Token{TokTypEOF, "", t.Pos}, nil
		} else if err != nil {
			return Token{TokTypInvalid, string(ch), t.Pos}, err
		}

		if ch != rune(' ') && ch != rune('\t') {
			break
		}
	}

	switch {
	case ch == rune('('):
		return Token{TokTypParenOpen, "(", t.Pos}, nil
	case ch == rune(')'):
		return Token{TokTypParenClose, ")", t.Pos}, nil
	case ch == rune('\''):
		return Token{TokTypQuote, "'", t.Pos}, nil
	case ch == rune('"'):
		return t.readString()
	case ch == rune('#'):
		return t.readHash()
	case ch >= rune('0') && ch <= rune('9'):
		return t.readNum(ch)
	default:
		return t.readSymbol(ch)
	}
}

func (t *Tokeniser) readChar(start uint) (Token, error) {
	full := make([]rune, 0)

	for {
		t.Pos++
		ch, _, err := t.r.ReadRune()
		if err != nil {
			if err == io.EOF {
				t.Pos--
				break
			} else if err != nil {
				return Token{TokTypInvalid, string(ch), t.Pos}, err
			}
		}

		if _, found := delim[ch]; found && len(full) != 0 {
			err = t.r.UnreadRune()
			if err != nil {
				return Token{TokTypInvalid, string(ch), t.Pos}, err
			}

			t.Pos--
			break
		}

		full = append(full, ch)
	}

	return Token{TokTypChar, `#\` + string(full), start}, nil
}

func (t *Tokeniser) readHash() (Token, error) {
	start := t.Pos

	ch, _, err := t.r.ReadRune()
	if err == io.EOF {
		return Token{TokTypInvalid, "", t.Pos}, errors.New("Unexpected EOF")
	} else if err != nil {
		return Token{TokTypInvalid, string(ch), t.Pos}, err
	} else if ch == '\\' {
		t.Pos++
		return t.readChar(start)
	}
	err = t.r.UnreadRune()
	if err != nil {
		return Token{TokTypInvalid, string(ch), t.Pos}, err
	}

	full := make([]rune, 0)
	for {
		t.Pos++
		ch, _, err := t.r.ReadRune()
		if err != nil {
			if err == io.EOF {
				t.Pos--
				break
			} else if err != nil {
				return Token{TokTypInvalid, string(ch), t.Pos}, err
			}
		}

		if _, found := delim[ch]; found {
			err = t.r.UnreadRune()
			if err != nil {
				return Token{TokTypInvalid, string(ch), t.Pos}, err
			}

			t.Pos--
			break
		}

		full = append(full, ch)
	}

	str := string(full)
	switch strings.ToLower(str) {
	case "t":
		fallthrough
	case "true":
		fallthrough
	case "f":
		fallthrough
	case "false":
		return Token{TokTypBool, "#" + str, start}, nil
	}

	return Token{TokTypInvalid, "#" + str, t.Pos}, nil
}

func (t *Tokeniser) readNum(first rune) (Token, error) {
	ret := make([]rune, 1)
	ret[0] = first
	start := t.Pos

	for {
		t.Pos++

		ch, _, err := t.r.ReadRune()
		if err == io.EOF {
			return Token{
					TokTypInvalid, "",
					t.Pos},
				errors.New("Unexpected EOF")
		} else if err != nil {
			return Token{TokTypInvalid, string(ch), t.Pos}, err
		}

		if _, found := delim[ch]; found {
			err = t.r.UnreadRune()
			if err != nil {
				return Token{TokTypInvalid, string(ch), t.Pos}, err
			}

			t.Pos--

			str := strings.ToLower(string(ret))
			_, err := strconv.ParseInt(str, 10, 0)
			if err == nil {
				return Token{TokTypInt, str, start}, err
			}
			_, err = strconv.ParseFloat(str, 10)
			if err == nil {
				return Token{TokTypFloat, str, start}, err
			}
			return Token{TokTypInvalid, str, start}, err
		}

		ret = append(ret, ch)
	}
}

func (t *Tokeniser) readString() (Token, error) {
	ret := make([]rune, 1)
	ret[0] = '"'
	start := t.Pos

	for {
		t.Pos++

		ch, _, err := t.r.ReadRune()
		if err == io.EOF {
			return Token{
				TokTypInvalid,
				string(ch),
				t.Pos,
			}, errors.New("Unexpected EOF")
		} else if err != nil {
			return Token{TokTypInvalid, string(ch), t.Pos}, err
		}

		ret = append(ret, ch)
		if ch == rune('"') {
			return Token{TokTypString, string(ret), start}, nil
		}
	}
}

func (t *Tokeniser) readSymbol(first rune) (Token, error) {
	ret := make([]rune, 1)
	ret[0] = first
	start := t.Pos

	for {
		t.Pos++

		ch, _, err := t.r.ReadRune()
		if err == io.EOF {
			t.Pos--
			return Token{TokTypSymbol, string(ret), start}, nil
		} else if err != nil {
			return Token{TokTypInvalid, string(ch), t.Pos}, err
		}

		if _, found := delim[ch]; found {
			err = t.r.UnreadRune()
			t.Pos--
			return Token{TokTypSymbol, string(ret), start}, err
		}

		ret = append(ret, ch)
	}
}
